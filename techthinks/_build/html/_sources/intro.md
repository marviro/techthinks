# General information about this course

## Teaching outcomes  

Technology thinks. A format, a protocol, an algorithm or a particular way of structuring and exposing data implements and spreads specific epistemological models, ways of seeing the world, values, ethical and political visions. This question is particularly evident when it comes to choosing formats and protocols for structuring and disseminating humanities research data, or cultural or heritage data. To understand these issues, it is necessary to develop a deep theoretical reflection that can only be based on a precise understanding of technical choices, formats, protocols and infrastructures.

To develop such a critical awareness, this course will focus in particular on the question of structuring cultural and heritage data and even more precisely on open data.
Since the late 1990s, the academic community and civil society have called for the emergence of the knowledge commons. The promotion of open access, open data, and open content has profoundly changed the way researchers and cultural organizations work.
The production of structured data and its exposure via APIs is increasingly common in the world of research in the humanities, as well as for cultural and heritage institutions. How should data be structured? How to make it accessible? And also: how to use it?

This course will lay the groundwork for understanding the issues involved in structuring cultural and heritage data with a focus on open data. The course will be structured in two parts, one theoretical and one practical. In the theoretical part, students will be introduced to the theoretical and epistemological issues of data structuring, the question of formats, paradigms and models behind the different technical choices, as well as the question of licenses and different forms of dissemination. The practical part of the course will include an introduction to the use of Python, a programming language that will be used to query APIs and to learn the basics of using digital resources.

At the end of the course, students will acquire the following skills

- understanding of the theoretical and epistemological issues of data structuring
- basic knowledge of Python
- understanding of the functioning of an API and of the main data exposure formats.

The class will be addressed to all the students of the lauree magistrali.


## Syllabus

The course will be divided into 7 sessions.

- 2024-02-19 (11am-1pm Laboratorio Coronelli) Introduction and theoretical stakes: how technology thinks
- 2024-02-26 (11am-1pm Laboratorio Coronelli) What is data? What is the difference between documents and data? What is open data?
- 2024-03-04 (this session will probably be moved to Friday the 8th: to be determined with the students) Formats, structures and epistemological models: how a particular data structure, format or protocol carries and implements a worldview. Licenses and legal considerations: how can data be open?
- 2024-03-11 (11am-1pm Laboratorio Coronelli) A (very short) introduction to Python
- 2024-03-18 (11am-1pm Laboratorio Coronelli) An introduction to APIs
- 2024-03-25 (11am-1pm Laboratorio Coronelli) Practical exemples: using open data
- 2024-04-08 (4 hours: 9am-1pm Laboratorio O Di Vita) Collective work and conclusions


## Evaluation methods


Each student will be asked to complete a small project using open data.

The project can be done with a Jupyter-notebook and should include:

- A short presentation of the project and the question to be asked
- A part of data recovery
- A part of data normalization (if necessary)
- A part of data exploitation: the data answer the question

The work will be evaluated on the basis of the relevance of the theoretical reflection and the coherence between the theoretical question and the technical implementation.

## Reference bibliography

- The Open Data Handbook (https://opendatahandbook.org/guide/en/)
- Meunier, Jean-Guy. « Humanités numériques ou computationnelles : Enjeux herméneutiques ». Sens Public, décembre 2014. http://www.sens-public.org/article1121.html.
- Mattingly, William. Introduction to Python for Digital Humanities, 2022. python-textbook.pythonhumanities.com.

