# Introduction and theoretical stakes: how technology thinks


<iframe width="100%" height="450px" marginheight="0" marginwidth="0" src="http://vitalirosati.net/slides/2024/cours-2024-techthinksumc.html">
  Slides
</iframe>


## Matter and thinking

>“Yet another one who, instead of working, wastes his time playing with LaTeX”.




## Plotinus and Porphiry

>I myself, Porphyry of Tyre, was one of Plotinus' very closest friends, and it was to me he entrusted the task of revising his writings. Such revision was necessary: Plotinus could not bear to go back on his work even for one re-reading; and indeed the condition of his sight would scarcely allow it: his handwriting was slovenly; he misjoined his words; he cared nothing about spelling; his one concern was for the idea: in these habits, to our general surprise, he remained unchanged to the very end.
He used to work out his design mentally from first to last: when he came to set down his ideas, he wrote out at one jet all he had stored in mind as though he were copying from a book. Interrupted, perhaps, by someone entering on business, he never lost hold of his plan; he was able to meet all the demands of the conversation and still keep his own train of thought clearly before him; when he was fee again, he never looked over what he had previously written–his sight, it has been mentioned, did not allow of such re-reading–but he linked on what was to follow as if no distraction had occurred. 


## Women and computers

- Melissa Terras, [For Ada Lovelace Day – Father Busa’s Female Punch Card Operatives, 2013, online](https://melissaterras.org/2013/10/15/for-ada-lovelace-day-father-busas-female-punch-card-operatives/)
- [Isabelle Collet, « La disparition des filles dans les études d’informatique : les conséquences d’un changement de représentation », Carrefours de l’education n° 17, nᵒ 1 (2004): 42‑56.](https://www.cairn.info/revue-carrefours-de-l-education-2004-1-page-42.html)

## Deconstructing Plato?

- Jacques Derrida, « La pharmacie de Platon », Tel Quel, nᵒ 32,33 (1968).

## New materialisms and  Karen Barad

- [Christopher N. Gamble, Joshua S. Hanan, et Thomas Nail, « What Is New Materialism? », Angelaki 24, nᵒ 6 (2 novembre 2019): 111‑34](https://doi.org/10.1080/0969725X.2019.1684704)
- Karen Barad, _Meeting the Universe Halfway: Quantum Physics and the Entanglement of Matter and Meaning_, (Durham: Duke University Press Books, 2007).



## Double slit

![](http://vitalirosati.net/slides/img/double-slit2.png)


Thomas Young, 1801: is the light a wave or a set of particles?


![](http://vitalirosati.net/slides/img/double-slit4.png)
![](http://vitalirosati.net/slides/img/double-slit3.png)
![](http://vitalirosati.net/slides/img/double-slit5.png)

>Bohr argued that if we were to perform a two-slit
experiment with a which-path device (which can be used to determine which
slit each electron goes through on its way to the detecting screen), we would
find that the interference pattern is destroyed. That is, if a measurement is
made that identifies the electron as a particle, as is the case when we use a
which-path detector, then the result will be a particle pattern, not the wave
pattern that results when the original unmodified two-slit apparatus is used.
[Karen Barad, _Meeting the Universe halfway_, p. 103]{.source}
:::

## Concepts are specific physical arrangements

>Bohr's argument for the indeterminable nature of measurement interactions is based on his insight that concepts are defined by the circumstances required
for their measurement.  That is, theoretical concepts are not ideational in character; they are specific physical arrangements. 
[Karen Barad, _Meeting the Universe halfway_, p. 109]{.source}

## Matter matters


## Writing...

- Tools, protocols, algorithms, formats… think. 
- How do they think? What do they think?


## Word 

- [Dehut, J. (2018). « En finir avec Word ! Pour une analyse des enjeux relatifs aux traitements de texte et à leur utilisation ». Carnet de recherche.](https://eriac.hypotheses.org/80)

- Kirschenbaum, M. G. (2016). Track Changes: A Literary History of Word Processing.
Cambridge, États-Unis d’Amérique: The Belknap Press of Harvard University Press


## A computer for writing?? 

Writing code and documentation, saving and **printing** it.

- 1976 Electric Pencil - Michael Shrayer
- 1979 Easy Writer - John Thomas Draper
- 1979 WordStar: WYSYWYG - for the concept of page
- Home computers - 1984
- 1983 Word

## The principles

- graphic interface
- printing is the goal!
- office! 
- format=software

## The effects

- WYSIWYG and "disintermediation"
  - loss of skills
- loss of control
- poor information
- loss of utility (using a computer as a typewriter) 
- sustainability issues
- **a formatted way of thinking**


1. The principle of WYSIWYG (_What You See Is What You Get_) determines a confusion between structuring and formatting: to facilitate the reading and processing of an article or a book, it is necessary to dissociate the semantic value of the fragments from their graphic rendering, for example: a level 2 title and its graphic rendering in a large font size,
2. The syntax is hidden: it is thus impossible to know what is going on. The software decides about the meaning!
2. The docx format does not allow a scientific structuring of the contents. The contents produced in docx are poorly marked up and difficult to access, as they are poorly indexed. At a time when there is a huge production of texts, it becomes crucial that documents are correctly structured to improve their indexing anda allow their query by advanced search tools.
3. Editors are obliged to do a long and tedious work of reworking texts in order to reintroduce texts in order to reintroduce a semantic layer and to correct the numerous and correct the numerous errors introduced by these word processors.
4. The durability of documents in docx format depends on Microsoft's willingness to maintain this
Microsoft to maintain this proprietary format. However, durability is not the mission and priority of a company.
mission and priority of a commercial enterprise. There is no guarantee that
content in docx format will be accessible in the future
5. **But the most serious thing is that writing and thinking become a single, homogeneous activity.  We all think the same way. And in all our activities we think in the same way: whether we are writing a love letter, a shopping list, a scientific article in physics or literature, a novel, lecture notes...

The only way to write seems to be the one used to write internal documents for a small company.**



