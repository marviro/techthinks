# Formats

The "standard way that information is encoded for storage in a computer file".

It means:

- a theoretical model
- an implementation

## Formats types

- open/closed
- free/proprietary

## Text formats

First of all, what is text?

Understand the difference between plain text and rich text:

- Plain text is chain of _encoded_ characters
- Rich text is text with the addition of metainformation (style, font, colors, page layouts...)

Plain text is encoded in the sense that there is a table of correspondences between each character and a series of binary numbers.

## ASCII

A standard way of encoding text:

![](ascii_table_lge.png)


ASCII can encode only 128 characters. This is why there are other standards. The most  widely used one is UTF-8.

## Rich Text horrors

![](traitement-de-texte-rouge.png)


## SGML, HTML, XML

- 1980: Standard Generalized Markup Language
- 1989: HTML (Hypertext Markup Language), Tim Bernbers-Lee
- 1996: XML (eXtensible Markup Language)


## TeX


