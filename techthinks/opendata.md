# Open Data

## What is Open Data?

:::{admonition} Definition
Open data is digital data that is accessible, usable, and documented.
:::

- Knowledge production and the importance of accessibility.
- Accessibility of not only documents but also data
- The dream of knowledge.

## Documents and Data

Who produces Open Data?

- Public entities  - Governments  - Public institutions  - ...
- Research institutions (public or private)
- Others (individuals, private entities...)

## The Ethical Question

- In the field of public data
- In the field of research data

The question of open access

## Characteristics of Open Data

- Accessible
- Relevant
- Documented
- Structured
- Visible
- Linked?

or, according to the Open Knowledge foundation: https://opendatahandbook.org/guide/en/what-is-open-data/:


- Availability and Access: the data must be available as a whole and at no more than a reasonable reproduction cost, preferably by downloading over the internet. The data must also be available in a convenient and modifiable form.
- Re-use and Redistribution: the data must be provided under terms that permit re-use and redistribution including the intermixing with other datasets.
- Universal Participation: everyone must be able to use, re-use and redistribute - there should be no discrimination against fields of endeavour or against persons or groups. For example, ‘non-commercial’ restrictions that would prevent ‘commercial’ use, or restrictions of use for certain purposes (e.g. only in education), are not allowed.


    
## Open Data Principles

- Complete
- Primary
- Up-to-date
- Accessible
- Machine-usable
- Non-discriminatory
- Free

Cf [here](https://opendatapolicies.org/guidelines/)

## Issues

<div style="max-width:854px"><div style="position:relative;height:0;padding-bottom:56.25%"><iframe src="https://embed.ted.com/talks/lang/en/ben_wellington_how_we_found_the_worst_place_to_park_in_new_york_city_using_big_data" width="854" height="480" style="position:absolute;left:0;top:0;width:100%;height:100%" frameborder="0" scrolling="no" allowfullscreen></iframe></div></div>

    Data accessibility
    Well-structured data  - How is it accessible? Do we search a disk if we ask?  - pdf  - csv  - api  - Structure = design epistemological paradigms  - Standardized or not? Standardization

## What formats?

- csv
- xml
- json
- ...

